# A student with student ID number au173126 has made a python script called "data_loggerWin.py", which
# can log data from an axial turbine made for educational use. The script was provided by the supervisor
# on this project. The script has been used as a base and then changes have been made, which will be
# highlighted.

# Update1 by Sleiman Farah (16/08/2023)
# Update2 by Sleiman Farah (04/09/2023)

import datetime
import os
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
import serial
import platform
import serial.tools.list_ports

ports = list(serial.tools.list_ports.comports())

# **********************************************************************************************************************
# Update2:
# This update is to identify the COM port for Mac.
# **********************************************************************************************************************

if platform.system() != "Windows":
    ii = 1
    n_port_max = 100000
    port_identified = False
    while not port_identified and ii < n_port_max:
        try:
            print(ii)
            ser = serial.Serial('/dev/cu.usbmodem' + str(ii), baudrate=9600, timeout=1)
            # ser = serial.Serial('/dev/tty.usbserial-' + str(ii))
        except:
            ii += 1
        else:
            port_identified = True
        finally:
            if not port_identified and ii == n_port_max:
                print("*********************************************************************************************")
                print("Connection port could not be identified. Check connection between computer and arduino board!")
                print("*********************************************************************************************")
                sys.exit(1)

# **********************************************************************************************************************
# Update1:
# This update identifies the COM port automatically.
# **********************************************************************************************************************
# Identify and open serial port.
else:
    ii = 1
    n_port_max = 10
    port_identified = False
    while not port_identified and ii < n_port_max:
        try:
            ser = serial.Serial('COM' + str(ii), 9600)
        except:
            ii += 1
        else:
            port_identified = True
        finally:
            if not port_identified and ii == n_port_max:
                print("*********************************************************************************************")
                print("Connection port could not be identified. Check connection between computer and arduino board!")
                print("*********************************************************************************************")
                sys.exit(1)

# **********************************************************************************************************************

# Added the following line because the logged data did not match the real time data because old data
# was logged out of the queue on the microcontroller board. The added line tells the serial port to
# clear the queue on the microcontroller board so the logged data matches with what can be seen on the
# LCD screen on the test stand.
ser.flushInput()
# Checks which port was really used
print(ser.name)
pause_time = 0.1
# The following number can be changed to suit the desired measurement period.
samples = 2*5 + 1
# Array with the time samples
t = np.arange(samples)
# An array created where data regarding RPM can be stored
RPM_arr = np.zeros(samples)
# Added the following line to the original script, so data regarding gauge pressure can be stored in
# an array.
gauge_pressure_arr = np.zeros(samples)
# Added the following line to the original script, so data regarding differential pressure can be
# stored in an array.
diff_pressure_arr = np.zeros(samples)
# Added the following line to the original script, so data regarding temperature from the differential
# pressure sensor can be stored in an array.
temperature_arr = np.zeros(samples)


# ******************************
# Added the following line to the original script, so data regarding temperature from the differential
# pressure sensor can be stored in an array.
v_arr = np.zeros(samples)


plt.close('all')

# The following line enable interactive mode, which shows/updates the figure after every plotting
# command
plt.ion()
# The following code deviates quite a bit from the original script since the original code made a graph
# showing RPM and power against the sample number. This has been changed, so known a plot with three
# graphs shows RPM vs. sample number, gauge pressure vs. sample number and differential pressure vs.
# sample number.
# Define three subplots
fig, axs = plt.subplots(1, 3)
# Define the size of the plotting window.
fig.set_size_inches(17, 7)
# Define the main title of the subplots
fig.suptitle('Radial compressor - measured data')
i = 0
time.sleep(3)
while i < samples:
    # Reading data from the serial port and storing the data in the correct arrays
    line = ser.readline()
    line_arr = line.split(b'\t')
    # [x.split(b’:’) for x in line_arr[:-1]]
    RPM_arr[i] = float(line_arr[0].strip(b'rpm:'))
    gauge_pressure_arr[i] = float(line_arr[1].strip(b'pressure_bar:'))
    diff_pressure_arr[i] = float(line_arr[2].strip(b'DifferentialPressure[Pa]:'))
    temperature_arr[i] = float(line_arr[3].strip(b'Temperature[degree]:'))
    # v_arr[i] = float(line_arr[4].strip(b'Velocity[m_per_sec]:'))
    # Plots the RPM from the test stand in subplot 1
    axs[0].plot(t, RPM_arr, 'tab:red', label='RPM')
    axs[0].axvline(i, ls='--', color='k')
    # Plots the gauge pressure from the test stand in subplot 2
    axs[1].plot(t, gauge_pressure_arr, 'tab:green', label='bar')
    axs[1].axvline(i, ls='--', color='k')
    # Plots the differential pressure from the test stand in subplot 3
    axs[2].plot(t, diff_pressure_arr, 'tab:orange', label='Pa')
    axs[2].axvline(i, ls='--', color='k')
    # Adding labels to the x and y axes of the subplots
    axs[0].set(xlabel='Sample number', ylabel='RPM')
    axs[1].set(xlabel='Sample number', ylabel='bar')
    axs[2].set(xlabel='Sample number', ylabel='Pa')
    plt.show()
    plt.pause(pause_time)
    axs[0].clear()
    axs[1].clear()
    axs[2].clear()
    # Next time step
    i += 1

# Close serial port
ser.close()
# Get the current date and time
current_datetime = datetime.datetime.now()

# Get the current date and time
current_datetime = datetime.datetime.now()

# Format the date and time as strings
formatted_date = current_datetime.strftime("%Y-%m-%d")
formatted_time = current_datetime.strftime("%H-%M-%S")

# Create a directory for the current date
date_directory = os.path.join("output", formatted_date)
os.makedirs(date_directory, exist_ok=True)

# Create a directory for the current time inside the date directory
time_directory = os.path.join(date_directory, formatted_time)
os.makedirs(time_directory, exist_ok=True)

impeller_type = "backward_10"
outlet_diameter_mm = "10"
file_name = time_directory + "/impeller_" + impeller_type + "_outlet_diameter_mm_" + outlet_diameter_mm + "_rpm_" + str(np.round(RPM_arr.mean(), 1))
# Added this line in order to save the logged data in a csv file. comments='' is added so the header is without #
np.savetxt(file_name + '.csv', np.c_[RPM_arr[1:], gauge_pressure_arr[1:], diff_pressure_arr[1:], temperature_arr[1:]], delimiter=',', header="RPM,Gauge pressure [bar],Differential pressure [Pa],Temperature[Degree]", comments='')